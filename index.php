<?php

$curl = curl_init();
curl_setopt($curl,CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result,true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$TotalConfirmed =(string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$TotalDeaths =(string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$TotalRecovered =(string)$global['TotalRecovered'];
$countries =$result['Countries'];

$temp_arr = $TotalDeaths/$TotalConfirmed*100;

?>
<!doctype html>
<html lang="en">
  <head>
   <meta charset="utf-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
            integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/a540d7261a.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
      .country{
        overflow: hidden;
        overflow-y: auto;
        height: 1050px;
      }
      .body{
        background:#111112;
      }
      .card-body-icon {
            position: absolute;
            z-index: 0;
            top: 25px;
            right: 1px;
            opacity: 0.4;
            font-size: 50px;
        }
          </style>
          <title>dashboard</title>
        </head>
        <body class="bg-light">
       <div class="body">
        <div class="container-fluid bg-dark text-light  position-fixed" style="z-index:999;">
        <div class="row">
          <div class="col">
          <nav class="navbar navbar-expand-lg navbar-secondary bg-dark">
            <a class="navbar-brand" href="#"><i class="fas fa-hand-sparkles text-light"></i></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="container">
                  <div class="collapse navbar-collapse text-center" id="navbarNavAltMarkup">
                <div class="navbar-nav"><h1 class="text-lightr">WHO Corona-19 Virus Dashboard According to the World</h1>
              </div>
           </div>
           </div>
         </nav>
        <h6 class="text-center text-light">Update Globally, as of <?php echo $countries[77]['Date'];?></h6>
      </div>
     </div>
    </div>
    <br>
    <br>
    <br>
    <br>
      <div class="row">
        <div class="col-2 my-2">
          <div class="card-header bg-dark text-light border">
            Country/191
            </div>
            <div class="country bg-secondary border pl-3">
            <?php foreach($countries as $key=>$value):?>
            <ul  class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
            <a class="nav-link text-light bg-secondary" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $value['Country'];?>
            </a>
              <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
              <p class="text-light">confirmed:<?php echo $value['TotalConfirmed'];?></p>
              <p class="text-light">new confirmed:<?php echo $value['NewConfirmed'];?></p>
              <p class="text-light" >recoverd:<?php echo $value['TotalRecovered']; ?></p>
              <p class="text-light" >new recoverd:<?php echo $value['NewRecovered']; ?></p>
              <p class="text-light">Death:<?php echo $value['TotalDeaths'];?></p>
              <p class="text-light">new Death:<?php echo $value['NewDeaths'];?></p>
              </div>
              </li>
              </ul>
             <?php endforeach;?>
            </div>
            </div>
            <div class="col-10">
            <div class="container">
             <div class="row">
              <div class="col-4 my-2">
                <div class="card-header bg-dark text-light border">
                    <h3>Total Recovered</h3>
                </div>
             <div class="card" >
              <div class="card-body bg-secondary ">
                <div class="card-body-icon"><i class="fas fa-hospital-user mr-3 text-warning"></i></div>
                  <h3 class="text-light "><?php echo number_format($TotalRecovered);?>.</h3>
                     <h6 class="text-light">new <?php echo number_format($temp_NewRecovered);?>.</h6>
                    </div>
                  </div>
                </div>
                <div class="col-4 my-2">
                  <div class="card-header bg-dark text-light border">
                    <h3>Total Confirmed</h3>
                </div>
                  <div class="card" >
                    <div class="card-body bg-secondary "> 
                      <div class="card-body-icon"><i class="fas fa-user-shield mr-3 text-warning"></i></div>
                      <h3 class="text-light "><?php echo number_format($TotalConfirmed);?>.</h3>
                      <h6 class="text-light">new <?php echo number_format($temp_NewConfirmed);?>.</h6>
                    </div>
                  </div>
                </div>
                <div class="col-4 my-2">
                <div class="card-header bg-dark text-light border">
                    <h3> Total Deaths</h3>
                </div>
                  <div class="card" >
                    <div class="card-body bg-secondary ">
                      <div class="card-body-icon"><i class="fas fa-users-slash text-warning"></i></div>
                      <h3 class="text-light"><?php echo number_format($TotalDeaths);?>.</h3>
                      <h6 class="text-light"> new <?php echo number_format($temp_NewDeaths);?>.</h4> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container-fluid">
              <div class="row d-flex justify-content-center">
                <div class="col-12 text-light">
                     <h3 style="text-align: center; font-size: 2vw;" class="mt-3">Chart Confirmed Covid-19</h3>
                   <canvas width="200" height="100" id="confirm"></canvas>
                 </div>
                <div class="col-6 text-light">
                    <h3 style="text-align: center; font-size: 2vw;" class="mt-3">Chart Death Covid-19</h3>
                      <canvas id="death" class="mt-3"></canvas>
                       </div>
                         <div class="col-6 text-light">
                        <h3 style="text-align: center; font-size: 2vw;" class="mt-3">Chart Recovery Covid-19</h3>
                     <canvas id="recovery" class="mt-3"></canvas>
                 </div>
                </div>
              </div>
            </div>
         </div>  
       </div>
       <!-- script -->
       <script>
            var confirmed = document.getElementById('confirm').getContext('2d');
            var death = document.getElementById('death').getContext('2d');
            var recovery = document.getElementById('recovery').getContext('2d');
            
            var data = $.ajax({
                url: "https://api.covid19api.com/summary",
                cache: false
            })
            .done(function (covid) {
                
                function getCountries(covid) {
                    var temp_country=[];
                    
                    covid.Countries.forEach(function(el){
                        temp_country.push(el.Country);
                    })
                    return temp_country;
                }
                
                function getConfirmed(covid) {
                    var temp_confirmed=[];
                    
                    covid.Countries.forEach(function(el) {
                        temp_confirmed.push(el.TotalConfirmed)
                    })
                    return temp_confirmed;
                }
                
                function getDeath(covid) {
                    var temp_death = [];
                    
                    covid.Countries.forEach(function(el) {
                        temp_death.push(el.TotalDeaths)
                    })
                    return temp_death;
                }

                function getRecovery(covid) {
                    var temp_recovery = [];

                    covid.Countries.forEach(function(el) {
                        temp_recovery.push(el.TotalRecovered)
                    })
                    return temp_recovery;
                }
                
                var colors = [];
                function getRandomColor() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgb(" + r + "," + g + "," + b + ")";
                }     
                
                for (var i in covid.Countries) {
                    colors.push(getRandomColor());
                }
                
                var confirmPieChart = new Chart(confirmed,{
                    type: 'bar',
                    data: {
                        labels: getCountries(covid),
                        datasets: [{
                            backgroundColor: colors,
                            label: '# Total',
                            data: getConfirmed(covid),
                            borderWidth: 0.5
                        }]
                    }, 
                    options: {
                        legend: {
                            display: false,
                        }
                    }
                })

                var deathPieChart = new Chart(death,{
                    type: 'bar',
                    data: {
                        labels: getCountries(covid),
                        datasets: [{
                            backgroundColor: colors,
                            label: '# Total',
                            data: getDeath(covid),
                            borderWidth: 0.5
                        }]
                    }, 
                    options: {
                        legend: {
                            display: false,
                        }
                    }
                })

                var recoveryPieChart = new Chart(recovery,{
                    type: 'bar',
                    data: {
                        labels: getCountries(covid),
                        datasets: [{
                            backgroundColor: colors,
                            label: '# Total',
                            data: getRecovery(covid),
                            borderWidth: 0.5
                        }]
                    }, 
                    options: {
                        legend: {
                            display: false,
                        }
                    }
                })
            });
       </script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  </body>
</html>